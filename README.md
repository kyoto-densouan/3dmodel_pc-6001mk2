# README #

1/3スケールのNEC PC-6001mk2風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1983年7月1日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-6000%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA#PC-6001mkII)
- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/e/1ba53708331301d2c7dbfe88e85675f5)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001mk2/raw/3476bef4874a9f96db97b95c74c176962ce4ab58/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001mk2/raw/3476bef4874a9f96db97b95c74c176962ce4ab58/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001mk2/raw/3476bef4874a9f96db97b95c74c176962ce4ab58/ExampleImage.png)
